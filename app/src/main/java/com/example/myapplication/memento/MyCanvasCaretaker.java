package com.example.myapplication.memento;

import android.graphics.Canvas;
import android.graphics.PointF;

import com.example.myapplication.figures.Figure;
import com.example.myapplication.interfaces.Drawing;
import com.example.myapplication.utils.PointD;

import java.util.Stack;

public class MyCanvasCaretaker implements Drawing {
    private CanvasOriginator canvasOriginator;
    private Stack<Memento> history;

    public MyCanvasCaretaker() {
        canvasOriginator = new CanvasOriginator();
        history = new Stack<>();
    }

    public void drawFigure(Figure figure) {
        history.add(canvasOriginator.save());
        canvasOriginator.addFigure(figure);
    }

    public void moveFigure(Figure figure, PointD dir) {
        canvasOriginator.moveFigure(figure, dir);
    }

    public void save() {
        history.add(canvasOriginator.save());
    }

    public Figure raycast(PointD touch) {
        return canvasOriginator.raycast(touch);
    }

    @Override
    public void draw(Canvas canvas) {
        canvasOriginator.draw(canvas);
    }

    public void undo() {
        if (history.size() > 0) {
            canvasOriginator.restore(history.pop());
        }
    }
}
