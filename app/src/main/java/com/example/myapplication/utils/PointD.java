package com.example.myapplication.utils;

import androidx.annotation.NonNull;

public class PointD {
    public double x;
    public double y;

    public PointD(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public PointD(@NonNull PointD pointD) {
        this(pointD.x, pointD.y);
    }
}
