package com.example.myapplication;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.behaviours.CircleBehaviour;
import com.example.myapplication.behaviours.RectBehaviour;
import com.example.myapplication.behaviours.TriangleBehaviour;
import com.example.myapplication.utils.Math;
import com.example.myapplication.views.MyCanvas;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

public class MainActivity extends AppCompatActivity {
    private int selColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        selColor = Color.WHITE;
        onRect(null);
    }

    public void butColor_Click(final View view) {
        ColorPickerDialogBuilder
                .with(this)
                .setTitle("Choose color")
                .initialColor(selColor)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton("ok", new ColorPickerClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        view.setBackgroundColor(selectedColor);
                        ((Button)view).setTextColor(Math.negativeColor(selectedColor));
                        selColor = selectedColor;
                    }
                })
                .build()
                .show();
    }

    public void startDraw() {
        findViewById(R.id.radioButtonRect).setEnabled(false);
        findViewById(R.id.radioButtonCircle).setEnabled(false);
        findViewById(R.id.radioButtonTriangle).setEnabled(false);
        findViewById(R.id.radioButtonMove).setEnabled(false);
    }

    public void stopDraw() {
        findViewById(R.id.radioButtonRect).setEnabled(true);
        findViewById(R.id.radioButtonCircle).setEnabled(true);
        findViewById(R.id.radioButtonTriangle).setEnabled(true);
        findViewById(R.id.radioButtonMove).setEnabled(true);
    }

    public void onMove(View view) {
        findViewById(R.id.radioButtonRect).setEnabled(false);
        findViewById(R.id.radioButtonCircle).setEnabled(false);
        findViewById(R.id.radioButtonTriangle).setEnabled(false);
        ((MyCanvas)findViewById(R.id.plot)).switchMove();
    }


    public void onDraw(View view) {
        findViewById(R.id.radioButtonRect).setEnabled(true);
        findViewById(R.id.radioButtonCircle).setEnabled(true);
        findViewById(R.id.radioButtonTriangle).setEnabled(true);
        ((MyCanvas)findViewById(R.id.plot)).switchMove();
    }

    public void onRect(View view) {
        ((MyCanvas)findViewById(R.id.plot)).setDrawingBehaviour(new RectBehaviour(this));
    }

    public void onCircle(View view) {
        ((MyCanvas)findViewById(R.id.plot)).setDrawingBehaviour(new CircleBehaviour(this));
    }

    public void onTriangle(View view) {
        ((MyCanvas)findViewById(R.id.plot)).setDrawingBehaviour(new TriangleBehaviour(this));
    }

    public void butUndo_Click(View view) {
        ((MyCanvas)findViewById(R.id.plot)).undo();
    }

    public int getSelColor() {
        return selColor;
    }
}